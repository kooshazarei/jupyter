#politician
DONALD_TRUMP = 'realdonaldtrump'
DONALD_TRUMP_NAME = ' President Donald J. Trump'
DONALD_TRUMP_BIO = '45th President of the United States'
DONALD_TRUMP_COLOR = '#446fc4'
DONALD_TRUMP_TRAIN_SET = DONALD_TRUMP_NAME + DONALD_TRUMP + DONALD_TRUMP_BIO + 'president of the united state of america.'

BARACK_OBAMA = 'barackobama'
BARACK_OBAMA_NAME = ' Barack Obama'
BARACK_OBAMA_BIO = ''
BARACK_OBAMA_COLOR = '#7f2d9b'
BARACK_OBAMA_TRAIN_SET = BARACK_OBAMA + BARACK_OBAMA_NAME + BARACK_OBAMA_BIO + '44th President of the United States.'

EMMANUEL_MACRON = 'emmanuelmacron'
EMMANUEL_MACRON_NAME = ' Emmanuel Macron'
EMMANUEL_MACRON_BIO = 'Président de la République française.'
EMMANUEL_MACRON_COLOR = '#369b4f'
EMMANUEL_MACRON_TRAIN_SET = EMMANUEL_MACRON + EMMANUEL_MACRON_NAME + EMMANUEL_MACRON_BIO

THERESA_MAY = 'theresamay'
THERESA_MAY_NAME = ' Theresa May'
THERESA_MAY_BIO = ' Prime Minister of the United Kingdom and leader of the @Conservatives.'
THERESA_MAY_COLOR = '#9b366f'
THERESA_MAY_TRAIN_SET = THERESA_MAY + THERESA_MAY_NAME + THERESA_MAY_BIO

BORIS_JOHNSON = 'borisjohnsonuk'
BORIS_JOHNSON_NAME = ' Boris Johnson'
BORIS_JOHNSON_BIO = ' Prime Minister of the United Kingdom and @Conservatives leader.'
BORIS_JOHNSON_COLOR = '#9b366f'
BORIS_JOHNSON_TRAIN_SET = BORIS_JOHNSON + BORIS_JOHNSON_NAME + BORIS_JOHNSON_BIO


#news agencies
CNN = 'cnn'
CNN_NAME = 'CNN'
CNN_BIO = 'CNN  @CNNBusiness @CNNTravel @CNNStyle  @CNNPolitics @cnnvision'
CNN_COLOR = '#369b9b'
CNN_TRAIN_SET = CNN_NAME + CNN_NAME + CNN_BIO

FOXNEWS = 'foxnews'
FOXNEWS_NAME = ' Fox News'
FOXNEWS_BIO = ''
FOXNEWS_COLOR = '#72369b'
FOXNEWS_TRAIN_SET = FOXNEWS_NAME #+ FOXNEWS_NAME + FOXNEWS_BIO

BBC = 'bbcnews'
BBC_NAME = ' BBC News'
BBC_BIO = ''
BBC_COLOR = '#aa5d33'
BBC_TRAIN_SET = BBC_NAME# + BBC_NAME + BBC_BIO

REUTERS = 'reuters'
REUTERS_NAME = ' Reuters'
REUTERS_BIO = ''
REUTERS_COLOR = '#aa5d33'
REUTERS_TRAIN_SET = REUTERS_NAME# + BBC_NAME + BBC_BIO

# sportsmen
LEOMESSI = 'leomessi'
LEOMESSI_NAME = ' Leo Messi'
LEOMESSI_BIO = ''
LEOMESSI_COLOR = '#aa5d33'
LEOMESSI_TRAIN_SET = LEOMESSI + LEOMESSI_NAME + LEOMESSI_BIO

CRISTIANO_RONALDO = 'cristiano'
CRISTIANO_RONALDO_NAME = ' Cristiano Ronaldo'
CRISTIANO_RONALDO_BIO = ''
CRISTIANO_RONALDO_COLOR = '#aa5d33'
CRISTIANO_RONALDO_TRAIN_SET = CRISTIANO_RONALDO + CRISTIANO_RONALDO_NAME + CRISTIANO_RONALDO_BIO

RAFAEL_NADAL = 'rafaelnadal'
RAFAEL_NADAL_NAME = ' Rafa Nadal'
RAFAEL_NADAL_BIO = ''
RAFAEL_NADAL_COLOR = '#aa5d33'
RAFAEL_NADAL_TRAIN_SET = RAFAEL_NADAL + RAFAEL_NADAL_NAME + RAFAEL_NADAL_BIO

ROGER_FEDERER = 'rogerfederer'
ROGER_FEDERER_NAME = ' Roger Federer'
ROGER_FEDERER_BIO = ''
ROGER_FEDERER_COLOR = '#aa5d33'
ROGER_FEDERER_TRAIN_SET = ROGER_FEDERER + ROGER_FEDERER_NAME + ROGER_FEDERER_BIO

NOVAK_DJOKOVIC = 'djokernole'
NOVAK_DJOKOVIC_NAME = ' Novak Djokovic'
NOVAK_DJOKOVIC_BIO = ''
NOVAK_DJOKOVIC_COLOR = '#aa5d33'
NOVAK_DJOKOVIC_TRAIN_SET = NOVAK_DJOKOVIC + NOVAK_DJOKOVIC_NAME + NOVAK_DJOKOVIC_BIO

#musician
LADY_GAGA = 'ladygaga'
LADY_GAGA_NAME = ' Lady Gaga'
LADY_GAGA_BIO = ''
LADY_GAGA_COLOR = '#aa5d33'
LADY_GAGA_TRAIN_SET = LADY_GAGA + LADY_GAGA_NAME + LADY_GAGA_BIO

BEYONCE = 'Beyonce'
BEYONCE_NAME = ' Beyoncé'
BEYONCE_BIO = ''
BEYONCE_COLOR = '#aa5d33'
BEYONCE_TRAIN_SET = BEYONCE + BEYONCE_NAME + BEYONCE_BIO

TAYLOR_SWIFT = 'taylorswift'
TAYLOR_SWIFT_NAME = ' Taylor Swift'
TAYLOR_SWIFT_BIO = ''
TAYLOR_SWIFT_COLOR = '#aa5d33'
TAYLOR_SWIFT_TRAIN_SET = TAYLOR_SWIFT + TAYLOR_SWIFT_NAME + TAYLOR_SWIFT_BIO

ADELE = 'adele'
ADELE_NAME = ' Adele'
ADELE_BIO = ''
ADELE_COLOR = '#aa5d33'
ADELE_TRAIN_SET = ADELE + ADELE_NAME + ADELE_BIO

MADONNA = 'madonna'
MADONNA_NAME = ' Madonna'
MADONNA_BIO = ''
MADONNA_COLOR = '#aa5d33'
MADONNA_TRAIN_SET = MADONNA + MADONNA_NAME + MADONNA_BIO


#actors
LEONARDO_DICAPRIO = 'leonardodicaprio'
LEONARDO_DICAPRIO_NAME = ' Leonardo DiCaprio'
LEONARDO_DICAPRIO_BIO = ''
LEONARDO_DICAPRIO_COLOR = '#aa5d33'
LEONARDO_DICAPRIO_TRAIN_SET = LEONARDO_DICAPRIO + LEONARDO_DICAPRIO_NAME + LEONARDO_DICAPRIO_BIO


###############################
# lists
###############################

LIST_ALL_USERS = [
    DONALD_TRUMP , BARACK_OBAMA, EMMANUEL_MACRON, BORIS_JOHNSON, THERESA_MAY,
    CNN, FOXNEWS, BBC, REUTERS,
    LEOMESSI, CRISTIANO_RONALDO, RAFAEL_NADAL, ROGER_FEDERER, NOVAK_DJOKOVIC,
    LADY_GAGA, BEYONCE, TAYLOR_SWIFT, ADELE, MADONNA,
]


LIST_POLITICIAN = [DONALD_TRUMP , BARACK_OBAMA, EMMANUEL_MACRON, BORIS_JOHNSON, THERESA_MAY]
LIST_SPORTPLAYER = [LEOMESSI, CRISTIANO_RONALDO, RAFAEL_NADAL, ROGER_FEDERER, NOVAK_DJOKOVIC]
LIST_NEWSAGENCY = [CNN, FOXNEWS, BBC, REUTERS]
LIST_MUSICIAN = [LADY_GAGA, BEYONCE, TAYLOR_SWIFT, ADELE, MADONNA]


WWW_HASHTAG_LIST = [ 'donaldtrump', 'barackobama', 'emmanuelmacron', 'borisjohnson', 'theresamay',
                    'cristianoronaldo', 'leomessi', 'rafaelnadal', 'rogerfederer', 'novakdjokovic',
                    'ladygaga' , 'beyonce', 'taylorswift', 'adele', 'madonna',
                   ]

NAME_COMMUNITIES = ['Politician', 'Sport Player', 'Musician']

NAME_CLUSTER_V2 = ['C0_FAN_PAGE', 'C1_BOT']


###############################
# hashtags
###############################
HASHTAG_TRUMP = 'donaldtrump' 
HASHTAG_OBAMA = 'barackobama'
HASHTAG_MACRON = 'emmanuelmacron'
HASHTAG_JOHNSON = 'borisjohnson'
HASHTAG_MAY = 'theresamay'

HASHTAG_RONALDO = 'cristianoronaldo'
HASHTAG_MESSI = 'leomessi'
HASHTAG_NADAL = 'rafaelnadal'
HASHTAG_FEDERER = 'rogerfederer'
HASHTAG_JOKOVIC = 'novakdjokovic'

HASHTAG_GAGA = 'ladygaga'
HASHTAG_BEYONCE = 'beyonce'
HASHTAG_TAYLOR = 'taylorswift'
HASHTAG_ADELE = 'adele'
HASHTAG_MADONNA = 'madonna'

HASHTAG_POLITICIAN = [HASHTAG_TRUMP, HASHTAG_OBAMA, HASHTAG_MACRON, HASHTAG_JOHNSON, HASHTAG_MAY]
HASHTAG_SPORT = [HASHTAG_RONALDO, HASHTAG_MESSI, HASHTAG_NADAL, HASHTAG_FEDERER, HASHTAG_JOKOVIC]
HASHTAG_MUSICIAN = [HASHTAG_GAGA, HASHTAG_BEYONCE, HASHTAG_TAYLOR, HASHTAG_ADELE, HASHTAG_MADONNA]

HASHTAG_LIST = [ HASHTAG_POLITICIAN, HASHTAG_SPORT, HASHTAG_MUSICIAN]

###############################
# define colors
###############################
COLOR_POLITICIAN = '#3e80f0'
COLOR_NEWSAGENCY = '#32a840'
COLOR_SPORTSARTS = '#f56d25'
COLOR_MUSICIAN = '#1acedb'

#clusters
COLOR_C0 = '#446fc4'
COLOR_C1 = '#7f2d9b'
COLOR_C2 = '#369b4f'

COLORS_COMMUNITIES = [ COLOR_POLITICIAN, COLOR_SPORTSARTS, COLOR_MUSICIAN ]
COLORS_CLUSTERS = [ COLOR_C0, COLOR_C1, COLOR_C2 ]




###############################
# functions
###############################
def get_train_set(_username):
    
    train_set = ""
    
    if (_username == DONALD_TRUMP):
        train_set = DONALD_TRUMP_TRAIN_SET
    elif (_username == BARACK_OBAMA ):
        train_set = BARACK_OBAMA_TRAIN_SET
    elif (_username == EMMANUEL_MACRON):
        train_set = EMMANUEL_MACRON_TRAIN_SET
    elif (_username == THERESA_MAY):
        train_set = THERESA_MAY_TRAIN_SET
    elif (_username == BORIS_JOHNSON):
        train_set = BORIS_JOHNSON_TRAIN_SET
        
        
    elif (_username == CNN):
        train_set = CNN_TRAIN_SET
    elif (_username == FOXNEWS):
        train_set = FOXNEWS_TRAIN_SET
    elif (_username == BBC):
        train_set = BBC_TRAIN_SET
    elif (_username == REUTERS):
        train_set = REUTERS_TRAIN_SET
        
    elif (_username == LEOMESSI):
        train_set = LEOMESSI_TRAIN_SET
    elif (_username == CRISTIANO_RONALDO):
        train_set = CRISTIANO_RONALDO_TRAIN_SET
    elif (_username == RAFAEL_NADAL):
        train_set = RAFAEL_NADAL_TRAIN_SET
    elif (_username == ROGER_FEDERER):
        train_set = ROGER_FEDERER_TRAIN_SET
    elif (_username == NOVAK_DJOKOVIC):
        train_set = NOVAK_DJOKOVIC_TRAIN_SET
        
        
    elif (_username == LADY_GAGA):
        train_set = LADY_GAGA_TRAIN_SET
    elif (_username == BEYONCE):
        train_set = BEYONCE_TRAIN_SET
    elif(_username == TAYLOR_SWIFT):
        train_set = TAYLOR_SWIFT_TRAIN_SET
    elif(_username == ADELE):
        train_set = ADELE_TRAIN_SET
    elif(_username == MADONNA):
        train_set = MADONNA_TRAIN_SET
        

    else:
        print("other!")
        
    return train_set





import nltk
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import stopwords
import wordninja
from nltk.tokenize import word_tokenize
import string
from collections import OrderedDict
nltk.download('stopwords')
nltk.download('punkt')


def tokenizer(text):
    _text = wordninja.split(text)

    # more than 3characters
    text = ""
    for item in _text:
        if (len(item) >= 3):
            text += " " + item


    # split into words
    tokens = word_tokenize(text)

    # convert to lower case
    tokens = [w.lower() for w in tokens]

    # remove punctuation from each word
    table = str.maketrans('', '', string.punctuation)
    stripped = [w.translate(table) for w in tokens]

    # # remove remaining tokens that are not alphabetic
    # words = [word for word in stripped if word.isalpha()]
    # print(words)
        
    # remove duplicates
    stripped = list(OrderedDict.fromkeys(stripped))
    
    # stop word
    filtered_words = [word for word in stripped if word not in stopwords.words('english')]
    
#     print(filtered_words)
    return filtered_words


def cal_similarity(_train, _txt1):
    train_string = _train
    doc1 = _txt1

    # Construct the training set as a list
    train_set = [train_string, doc1]

    # Set up the vectoriser, passing in the stop words
    tfidf_vectorizer = TfidfVectorizer(tokenizer=tokenizer)

    # Apply the vectoriser to the training set
    tfidf_matrix_train = tfidf_vectorizer.fit_transform(train_set)

    # Print the score
    resutl = cosine_similarity(tfidf_matrix_train[0:1], tfidf_matrix_train)
    return resutl[0][-1]
    